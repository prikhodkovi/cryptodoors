const assert = require('assert');
const app = require('../../src/app');

describe('\'exchange_rates\' service', () => {
  it('registered the service', () => {
    const service = app.service('exchange-rates');

    assert.ok(service, 'Registered the service');
  });
});
