const web3Lib = require("web3");
const fs = require("fs");
const Tx = require('ethereumjs-tx')

var web3;
var contractsData;

function addTransaction(portfolioAddress, baseCurrency, marketCurrency, action, baseAmount, marketAmount, timestamp) {

  return new Promise(function (resolve, reject) {
    web3.eth.getGasPrice().then(function (gasPrice) {
        const gasPriceHex = web3.utils.toHex(gasPrice * 10);
        const gasLimitHex = web3.utils.toHex(3000000);
        web3.eth.getTransactionCount(account).then(function (nonce) {
          var tra = {
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            data: contract.methods.addTransaction(portfolioAddress, baseCurrency, marketCurrency, action, baseAmount, marketAmount, timestamp).encodeABI(),
            from: account,
            to: contractAddress,
            nonce: nonce
          };

          var tx = new Tx(tra);
          tx.sign(key);
          var stx = tx.serialize();
          web3.eth.sendSignedTransaction('0x' + stx.toString('hex'), function (err, hash) {
            if (err) {
              console.log(err);
              return;
            }
            console.log('contract creation tx: ' + hash);
          });
        });


      }
    );

  });

}


class Service {
  constructor(options) {
    web3 = new web3Lib(new web3Lib.providers.HttpProvider(options.web3Provider));
    const contractsJson = fs.readFileSync('ContractsData.json', 'utf8');
    contractsData = JSON.parse(contractsJson);
  }

  find(params) {

    if (params.query.portfolio != undefined) {
      return addTransaction(params.query.portfolioAddress,
        params.query.baseCurrency, params.query.marketCurrency,
        params.query.action, params.query.baseAmount, params.query.marketAmount,
        params.query.timestamp);
    }

    return Promise.resolve({});


  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
